console.log ("Hello Batch 170");

console.log ("David JOhn Dominic G. Verunque");


// mini activity
console.log ("Pizza");
console.log ("Burger");
console.log ("spaghetti");

let food1 = "Pizza";

console.log ("My favorite food is " + food1);

let food2 = "Burger";

console.log ("I like " + food1 + " and " + food2);

let food3;

console.log (food3);

food3 = "Spaghetti";

console.log (food3);

/* let food1 = "another food";
console.log (food1); */


const pi = 3.1416;
console.log(pi);

/* 
pi = "pizza";
console.log(pi); */


food1 = "another food";
food2 = "this is food2";
console.log(food1);
console.log(food2);

/* 

1. We can create alet variable with a let keyword. let variable can be reassigned but not redeclared.

2. Creating a variable has two parts: Declaration of the variable name; and initialization of the initial value 

*/


// Data Types
console.log ("Sample String Data");


// numeric data types
console.log (123456789);

let zero = 0;
console.log(zero);

console.log(1+1);


// Miniactivity

let fullName = "David John Dominic Verunque";
let birthday = "July 30, 1999";
let loc = "Villaba";

let score = 99;
let fav = 12;
let fan_fav = 1;

console.log(fullName);
console.log(birthday);
console.log(loc);

console.log(score);
console.log(fav);
console.log(fan_fav);


function printStar(){
	console.log("*")
};

printStar();
printStar();
printStar();
printStar();




function sayHello(name){
	console.log("Hello " + name)
};

sayHello("David");


// alert

/* function alertPrint(){
    alert("Hello");
    console.log("Hello");
}


alertPrint();
*/
function add(firstNum, secondNum) {
    let sum = firstNum + secondNum;
    console.log(sum);
}


add(4,10);

function printBio(fname, lname, age) {
    // console.log ("Hello " + fName + " " + lName + " " + age)
    console.log (`Hello ${fname} ${lname} ${age}`);
};

printBio ("David", "Verunque", 13);







function createFullName(fname, mname, lname) {
    return `${fname} ${mname} ${lname}`
    
}

let full_Name = createFullName ("David", "John", "Dominic");
console.log(full_Name);