// console.log ("Hello World");

let fname = "David";
let lname = "Verunque";
let age = 22;

console.log (`First Name:  ${fname}`);
console.log (`Last Name: ${lname}`);
console.log (`Age: ${age}`);

// Hobbies (console log)
console.log ("Hobbies:");
let hobby_1 = "reading manga";
let hobby_2 = "watching documentaries";
let hobby_3 = "youtube";
console.log ([hobby_1, hobby_2, hobby_3]);


// Work Address (console log)
console.log ("Work Address: ")
let street = "Rizal, ";
let brgy = "Fatima, ";
let city = "Villaba";
let province = "Leyte";
console.log ({street, brgy, city, province})


// FUNCTION

// bio
function bio (fname, lname, age) {
    console.log (`${fname} ${lname} is ${age} years old`)
    console.log ("This was printed inside of function")
};
bio ("David", "Verunque", 22);


// hobbies
function hobbies(hobby_1, hobby_2, hobby_3){
    console.log ([hobby_1, hobby_2, hobby_3])
    console.log ("This was printed inside of function")
}
hobbies ("reading manga", "watching documentaries", "youtube");



// address
console.log (`Work Address: `);

function workAddress(street, brgy, city, province, ismarried) {
    console.log ({street, brgy, city, province})
    console.log (`The value of ismarried is: ${ismarried}`)
}
workAddress ("rizal", "fatima", "villaba", "leyte", "No");


